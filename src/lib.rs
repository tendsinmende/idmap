/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

pub type Id = usize;

pub struct IdMap<T> {
    pub(crate) values: Vec<Option<T>>,
    ///tracks free ids
    pub(crate) free: Vec<usize>,
}

impl<T> IdMap<T> {
    pub fn new() -> Self {
        IdMap {
            values: Vec::new(),
            free: Vec::new(),
        }
    }

    pub fn push(&mut self, item: T) -> Id {
        if self.free.len() > 0 {
            let id = self.free.pop().unwrap();
            self.values[id] = Some(item);
            id
        } else {
            let id = self.values.len();
            self.values.push(Some(item));
            id
        }
    }

    pub fn remove(&mut self, id: Id) -> Option<T> {
        //There are two cases which we have to handle. First, being, that the id exists, but has no value (already empty),
        //second beings, someone passed a garbage id outside of our vec.
        match self.values.get(id) {
            Some(value) => {
                if value.is_none() {
                    return None;
                }
            }
            None => return None,
        }

        //valid id, therfore push and take out actual value
        self.free.push(id);
        self.values[id].take()
    }

    pub fn get(&self, id: Id) -> Option<&T> {
        if let Some(value) = self.values.get(id) {
            value.as_ref()
        } else {
            None
        }
    }

    pub fn get_mut(&mut self, id: Id) -> Option<&mut T> {
        if let Some(value) = self.values.get_mut(id) {
            value.as_mut()
        } else {
            None
        }
    }

    pub fn contains_key(&self, key: Id) -> bool {
        if key >= self.values.len() {
            false
        } else {
            //has a slot there, check if it is used
            self.values[key].is_some()
        }
    }

    pub fn len(&self) -> usize {
        self.values.len() - self.free.len()
    }

    pub fn iter<'a>(&'a self) -> IdMapIter<'a, T> {
        IdMapIter { map: &self, at: 0 }
    }

    pub fn iter_mut<'a>(&'a mut self) -> IdMapIterMut<'a, T> {
        IdMapIterMut { map: self, at: 0 }
    }
}

pub struct IdMapIter<'a, T> {
    map: &'a IdMap<T>,
    at: usize,
}

impl<'a, T> Iterator for IdMapIter<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<Self::Item> {
        if self.at >= self.map.values.len() {
            return None;
        }

        while self.map.values[self.at].is_none() {
            self.at += 1;
        }

        let item = self.map.values[self.at].as_ref();
        self.at += 1;
        item
    }
}

pub struct IdMapIterMut<'a, T> {
    map: &'a mut IdMap<T>,
    at: usize,
}

impl<'a, T: 'static> Iterator for IdMapIterMut<'a, T> {
    type Item = &'a mut T;
    fn next(&mut self) -> Option<Self::Item> {
        if self.at >= self.map.values.len() {
            return None;
        }

        while self.map.values[self.at].is_none() {
            self.at += 1;
        }

        let item: Option<&T> = self.map.get(self.at);
        self.at += 1;

        //SAFETY:
        //cast is okay here since the map has to be borrowed mutable already.
        //so we are the only reader.
        let mutitem: Option<&'a mut T> = unsafe { item.map(|i| &mut *(i as *const T as *mut T)) };

        mutitem
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn add_remove() {
        let mut map: IdMap<u32> = IdMap::new();
        let id0 = map.push(0);
        let id1 = map.push(1);
        let id2 = map.push(2);
        let id3 = map.push(3);
        let id4 = map.push(4);

        assert!(map.len() == 5);

        map.remove(id2);
        assert!(map.len() == 4);
        assert!(map.free[0] == id2);
    }

    #[test]
    fn iter_immutable() {
        let mut map: IdMap<u32> = IdMap::new();
        let _id0 = map.push(0);
        let _id1 = map.push(1);
        let _id2 = map.push(2);
        let _id3 = map.push(3);
        let _id4 = map.push(4);

        let mut t: u32 = 0;
        for i in map.iter() {
            assert!(*i == t);
            t += 1;
        }
    }

    #[test]
    fn iter_mutable() {
        let mut map: IdMap<u32> = IdMap::new();
        let id0 = map.push(0);
        let id1 = map.push(1);
        let id2 = map.push(2);
        let id3 = map.push(3);
        let id4 = map.push(4);

        for i in map.iter_mut() {
            *i += 1;
        }

        let mut t: u32 = 1;

        for i in map.iter() {
            assert!(*i == t);
            t += 1;
        }
    }
}
