# IdMap

Similar to a HashMap or TreeMap. But instead uses a simple vector internally. Also the key is always `usize`.

Use [slotmap](https://crates.io/crates/slotmap) for a better alternative.
